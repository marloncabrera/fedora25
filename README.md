### Fedora 25 Tips & Tricks

 - Add a network printer
 - 
 https://fedoraproject.org/wiki/How_to_debug_printing_problems
 
```
journalctl -u cups -e
```


```
Feb 17 17:55:19 galadriel cupsd[1745]: The printer is not responding.
Feb 17 17:55:49 galadriel cupsd[1745]: Connecting to fe80::21d:d9ff:fe43:506b:631
Feb 17 17:55:49 galadriel cupsd[1745]: Connecting to printer.
Feb 17 17:55:49 galadriel cupsd[1745]: Connection error: Invalid argument
Feb 17 17:55:49 galadriel cupsd[1745]: The printer is not responding.
```



Looks like CUPS is using IPV6 instead IPV4...

10 minutes after I disabled IPV6 on Fedora and Elementary, uninstall and re-install
the printer but still not working...

```
Feb 17 18:39:16 galadriel cupsd[838]: Sending data to printer.
Feb 17 18:39:16 galadriel cupsd[838]: update_reasons(attr=1(none), s=\"(null)\")
Feb 17 18:39:16 galadriel cupsd[838]: op=\' \', new_reasons=0, state_reasons=1
Feb 17 18:39:16 galadriel cupsd[838]: Get-Printer-Attributes: successful-ok (successful-ok)
Feb 17 18:39:16 galadriel cupsd[838]: (monitor) Get-Job-Attributes: successful-ok (successful-ok)
Feb 17 18:39:16 galadriel cupsd[838]: (monitor) job-state = processing-stopped
Feb 17 18:39:16 galadriel cupsd[838]: (monitor) job-state = processing-stopped
Feb 17 18:39:16 galadriel cupsd[838]: Sending data to printer.
Feb 17 18:39:16 galadriel cupsd[838]: update_reasons(attr=1(none), s=\"(null)\")
Feb 17 18:39:16 galadriel cupsd[838]: op=\' \', new_reasons=0, state_reasons=1
Feb 17 18:39:16 galadriel cupsd[838]: Get-Printer-Attributes: successful-ok (successful-ok)
Feb 17 18:39:16 galadriel cupsd[838]: Get-Job-Attributes: successful-ok (successful-ok)
Feb 17 18:39:16 galadriel cupsd[838]: update_reasons(attr=0(), s=\"+cups-remote-stopped\")
Feb 17 18:39:16 galadriel cupsd[838]: op=\'+\', new_reasons=1, state_reasons=1
Feb 17 18:39:16 galadriel cupsd[838]: PAGE: total 0
```

Solved with a firewall rule:

https://scottbanwart.com/blog/2013/09/viewing-remote-cups-printers-in-fedora-19/

```
sudo /usr/bin/firewall-cmd --permanent --add-service=ipp-client
```


Put this on Fedora pastebin as well:  https://paste.fedoraproject.org/paste/Aa2IonxMbE-enOdif2DCb15M1UNdIGYhyRLivL9gydE=

